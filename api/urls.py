from api.views import ApiViewSet
from django.urls import re_path

apiViewset = ApiViewSet.as_view({
    'get': 'loadAll',
})

urlpatterns = [
    re_path(r'^load/$', apiViewset, name='load'),
]
