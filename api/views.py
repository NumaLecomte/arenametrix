import json

from rest_framework.response import Response
from rest_framework import status, viewsets

from client.views import ClientViewSet
from client.serializers import ClientSerializer

from representation.views import RepresentationViewSet
from representation.serializers import RepresentationSerializer

from spectacle.views import SpectacleViewSet
from spectacle.serializers import SpectacleSerializer

from reservation.views import ReservationViewSet
from reservation.serializers import ReservationSerializer




class ApiViewSet(viewsets.ViewSet):

    def loadClients(self, request):
        clients = ClientViewSet.as_view({'get':'extractClients'})(request._request).data
        clientSerializer = ClientSerializer(data=json.loads(clients), many=True)
        if clientSerializer.is_valid():
            clientSerializer.save()
            return Response(clientSerializer.data, status=status.HTTP_200_OK)
        return Response(clientSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def loadRepresentations(self, request):
        representations = RepresentationViewSet.as_view({'get':'extractRepresentations'})(request._request).data
        representationSerializer = RepresentationSerializer(data=json.loads(representations), many=True)
        if representationSerializer.is_valid():
            representationSerializer.save()
            return Response(representationSerializer.data, status=status.HTTP_200_OK)
        return Response(representationSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def loadSpectacles(self, request):
        spectacles = SpectacleViewSet.as_view({'get':'extractSpectacles'})(request._request).data
        spectacleSerializer = SpectacleSerializer(data=json.loads(spectacles), many=True)
        if spectacleSerializer.is_valid():
            spectacleSerializer.save()
            return Response(spectacleSerializer.data, status=status.HTTP_200_OK)
        return Response(spectacleSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def loadReservations(self, request):
        reservations = ReservationViewSet.as_view({'get':'extractReservations'})(request._request).data
        reservationSerializer = ReservationSerializer(data=json.loads(reservations), many=True)
        if reservationSerializer.is_valid():
            reservationSerializer.save()
            return Response(reservationSerializer.data, status=status.HTTP_200_OK)
        return Response(reservationSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def loadAll(self, request):
        try:
            responseClient = self.loadClients(request)
            responseRepresentation = self.loadRepresentations(request)
            responseSpectacle = self.loadSpectacles(request)
            responseReservation = self.loadReservations(request)

            print(responseClient.status_code)
            print(responseRepresentation.status_code)
            print(responseSpectacle.status_code)
            print(responseReservation.status_code)
            return Response(0, status=status.HTTP_200_OK)
        except:
            return Response(1, status=status.HTTP_400_BAD_REQUEST)
