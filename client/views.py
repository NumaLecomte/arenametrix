import pandas as pd
import os
from rest_framework.views import csrf_exempt
from .serializers import ClientSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets


class ClientViewSet(viewsets.ViewSet):

    def extractClients(self, request):
        try:
            dir_path = os.path.abspath('./sample.csv')
            df = pd.read_csv(dir_path, sep=';')
            clients = self.getClientDF(df)
            return Response(clients, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e)

    def loadClients(self, request):
        serializer = ClientSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def getClientDF(self, df):
        columns = ['nom', 'prenom', 'email', 'adresse', 'codepostal', 'pays', 'age', 'sexe']
        df2 = pd.DataFrame(columns=columns)

        for index, row in df.iterrows():
            df2.loc[len(df2)] = {
                'email': row['Email'],
                'nom': row['Nom'],
                'prenom': row['Prenom'],
                'adresse': row['Adresse'],
                'codepostal': row['Code postal'],
                'pays': row['Pays'],
                'age': row['Age'],
                'sexe': row['Sexe']
            }

        df2[['age']] = df2[['age']].fillna(value=-1)
        df2[['sexe']] = df2[['sexe']].fillna(value='O')

        return df2.drop_duplicates('email').to_json(orient='records')
