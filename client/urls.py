from client.views import ClientViewSet
from django.urls import re_path


clientViewset = ClientViewSet.as_view({
    'get' : 'extractClients',
    'post': 'loadClients'
})


urlpatterns = [
    re_path(r'^clients/$', clientViewset, name='client-api')
]
