from django.db import models


class Client(models.Model):
    email = models.TextField(primary_key=True)
    nom = models.TextField()
    prenom = models.TextField()
    adresse = models.TextField()
    codepostal = models.IntegerField()
    pays = models.TextField()
    age = models.IntegerField(default=-1)
    sexe = models.CharField(max_length=1, default='O')
