from rest_framework import serializers
from .models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('email', 'nom', 'prenom', 'adresse', 'codepostal', 'pays', 'age', 'sexe')
