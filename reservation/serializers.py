from rest_framework import serializers
from .models import Reservation


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = ('numero_billet', 'reservation', 'date_de_reservation', 'cle_spectacle', 'cle_representation', 'email')
