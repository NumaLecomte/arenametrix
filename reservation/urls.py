from reservation.views import ReservationViewSet
from django.urls import re_path

reservationViewset = ReservationViewSet.as_view({
    'get':'extractReservations',
    'post':'loadReservations'
})

urlpatterns = [
    re_path(r'^reservations/$', reservationViewset, name='reservation-api')
]