import pandas as pd
import os
import datetime

from .serializers import ReservationSerializer
from rest_framework.response import Response
from rest_framework import status, viewsets


class ReservationViewSet(viewsets.ViewSet):
    def extractReservations(self, request):
        try:
            dir_path = os.path.abspath('./sample.csv')
            df = pd.read_csv(dir_path, sep=';')
            reservations = self.getReservation(df)
            return Response(reservations, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e)

    def loadReservations(self, request):
        serializer = ReservationSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def getReservation(self, df):
        columns = ['numero_billet', 'reservation', 'date_de_reservation', 'cle_spectacle', 'cle_representation', 'email']
        df2 = pd.DataFrame(columns=columns)

        for index, row in df.iterrows():
            df2.loc[len(df2)] = {
                'numero_billet': row['Numero billet'],
                'reservation': row['Reservation'],
                'date_de_reservation': self.getDateTime(row['Date reservation'], row['Heure reservation']),
                'cle_spectacle': row['Cle spectacle'],
                'cle_representation': row['Cle representation'],
                'email': row['Email']}

        return df2.drop_duplicates('numero_billet').to_json(orient='records')


    def getDateTime(self, dateString, hourString):
        date = datetime.datetime.strptime(dateString[:9] + ' ' + hourString, "%d/%m/%y %H:%M:%S")
        date2 = date.strftime('%Y-%m-%d %H:%M:%S')
        return date2


