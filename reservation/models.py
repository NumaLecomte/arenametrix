from django.db import models
from client.models import Client
from representation.models import Representation
from spectacle.models import Spectacle


class Reservation(models.Model):
    numero_billet = models.IntegerField(primary_key=True)
    reservation = models.IntegerField()
    date_de_reservation = models.DateTimeField()
    cle_spectacle = models.ForeignKey(Spectacle, on_delete=models.CASCADE)
    cle_representation = models.ForeignKey(Representation, on_delete=models.CASCADE)
    email = models.ForeignKey(Client, on_delete=models.CASCADE)
