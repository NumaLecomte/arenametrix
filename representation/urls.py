from representation.views import RepresentationViewSet
from django.urls import re_path


representationViewset = RepresentationViewSet.as_view({
    'get': 'extractRepresentations',
    'post': 'loadRepresentations'
})


urlpatterns = [
    re_path(r'^representations/$', representationViewset, name='representation-api')
]
