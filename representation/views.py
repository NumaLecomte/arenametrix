import os
import pandas as pd
import datetime

from rest_framework import viewsets, status
from .serializers import RepresentationSerializer
from rest_framework.response import Response


class RepresentationViewSet(viewsets.ViewSet):

    def extractRepresentations(self, request):
        try:
            dir_path = os.path.abspath('./sample.csv')
            df = pd.read_csv(dir_path, sep=';')
            representations = self.getRepresentationDF(df)
            return Response(representations, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(e)

    def loadRepresentations(self, request):
        serializer = RepresentationSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def getRepresentationDF(self, df):
        columns = ['cle_representation', 'representation', 'date_representation', 'date_fin_representation', 'prix', 'type_de_produit', 'filiere_de_vente']
        df2 = pd.DataFrame(columns=columns)

        for index, row in df.iterrows():
            df2.loc[len(df2)] = {
                'cle_representation': row['Cle representation'],
                'representation': row['Representation'],
                'date_representation': self.getDateTime(row['Date representation'], row['Heure representation']),
                'date_fin_representation': self.getDateTime(row['Date fin representation'], row['Heure fin representation']),
                'prix': row['Prix'],
                'type_de_produit': row['Type de produit'],
                'filiere_de_vente': row['Filiere de vente']}

        return df2.drop_duplicates('cle_representation').to_json(orient='records')

    def getDateTime(self, dateString, hourString):
        date = datetime.datetime.strptime(dateString[:9] + ' ' + hourString, "%d/%m/%y %H:%M:%S")
        date2 = date.strftime('%Y-%m-%d %H:%M:%S')
        return date2


