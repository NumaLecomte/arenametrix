from django.db import models


class Representation(models.Model):
    cle_representation = models.IntegerField(primary_key=True)
    representation = models.TextField()
    date_representation = models.DateTimeField()
    date_fin_representation = models.DateTimeField()
    prix = models.IntegerField()
    type_de_produit = models.TextField()
    filiere_de_vente = models.TextField()
