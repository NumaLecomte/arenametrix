from rest_framework import serializers
from .models import Representation


class RepresentationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Representation
        fields = ('cle_representation', 'representation', 'date_representation', 'date_fin_representation', 'prix', 'type_de_produit', 'filiere_de_vente')
