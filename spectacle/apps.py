from django.apps import AppConfig


class SpectacleConfig(AppConfig):
    name = 'spectacle'
