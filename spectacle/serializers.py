from rest_framework import serializers
from .models import Spectacle


class SpectacleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spectacle
        fields = ('cle_spectacle', 'spectacle')
