import pandas as pd
import os
import json
from .serializers import SpectacleSerializer
from rest_framework.response import Response
from rest_framework import status, viewsets


class SpectacleViewSet(viewsets.ViewSet):

    def extractSpectacles(self, request):
        try:
            dir_path = os.path.abspath('./sample.csv')
            df = pd.read_csv(dir_path, sep=';')
            spectacles = self.getSpectacleDF(df)
            return Response(spectacles, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(json.load(e))

    def loadSpectacles(self, request):
        serializer = SpectacleSerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def getSpectacleDF(self, df):
        columns = ['cle_spectacle', 'spectacle']
        df2 = pd.DataFrame(columns=columns)

        for index, row in df.iterrows():
            df2.loc[len(df2)] = {
                'cle_spectacle': row['Cle spectacle'],
                'spectacle': row['Spectacle']
            }

        return df2.drop_duplicates('cle_spectacle').to_json(orient='records')
