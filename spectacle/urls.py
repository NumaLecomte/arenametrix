from spectacle.views import SpectacleViewSet
from django.urls import re_path

spectacleViewset = SpectacleViewSet.as_view({
    'get':'extractSpectacles',
    'post':'loadSpectacles'
})

urlpatterns = [
    re_path(r'^spectacles/$', spectacleViewset, name='spectacle-api')
]
