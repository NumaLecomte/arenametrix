from django.db import models


class Spectacle(models.Model):
    cle_spectacle = models.IntegerField(primary_key=True)
    spectacle = models.TextField()
