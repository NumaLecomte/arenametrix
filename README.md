## Installation

virtualenv arena

cd arena/bin

source activate

git clone https://gitlab.com/NumaLecomte/arenametrix.git

cd arenametrix

pip install -r requirements.txt

## setup de la base de donnée

ici ma base tourne en local, son nom est arenametrix

donc ouvrez un shell postgres ou utilisez le binaire createdb :

createdb arenametrix

ensuite ouvrez le fichier settings.py du server pour modifier le username
(il correspond au username ayant créé la base dans mon cas : numa)

DATABASES = {<br>
    'default': {<br>
        'ENGINE': 'django.db.backends.postgresql',<br>
        'NAME': 'arenametrix',<br>
---> à modifier        'USER': 'numa',<br>
        'PASSWORD': '',<br>
        'HOST': 'localhost',<br>
        'PORT': '',<br>
    }<br>
}<br>

## Run server
python manage.py migrate<br>
python manage.py runserver

## Api

#### login 

POST http://localhost:8000/login + username, password<br><br>
retourne un token à utiliser dans le header des requêtes suivantes

#### extraction et import des données dans une base postgres

GET http://localhost:8000/load

#### Api Client

GET http://localhost:8000/clients<br><br>
POST http://localhost:8000/clients

#### Api Representation

GET http://localhost:8000/representations<br><br>
POST http://localhost:8000/representations

#### Api Spectacle

GET http://localhost:8000/spectacles<br><br>
POST http://localhost:8000/spectacles

#### Api Reservaton

GET http://localhost:8000/reservations<br><br>
POST http://localhost:8000/reservations

### recap des statistique

http://localhost:8000/stats

### stats + filtre sur les dates

Exemple : 

http://localhost:8000/stats/startDate/endDate

avec startDate et endDate au format iso : 2015-6-2

http://localhost:8000/stats/2015-6-2/2015-6-4