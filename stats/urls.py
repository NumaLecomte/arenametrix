from .views import StatsViewset
from django.urls import re_path


statsViewset = StatsViewset.as_view({
    'get': 'getAllStats'
})

urlpatterns = [
    re_path(r'^stats/$', statsViewset, name='stats-api')
]