from client.models import Client
from reservation.models import Reservation
from representation.models import Representation
from django.db.models import Count, Avg, F
from rest_framework.response import Response
from rest_framework import status, viewsets


class StatsViewset(viewsets.ViewSet):

    def getReservationCount(self, request):
        count = Reservation.objects.count()
        return Response(count, status=status.HTTP_200_OK)

    def getCountUniqueBuyers(self, request):
        count = Reservation.objects.values('email_id').annotate(Count('email_id', distinct=True)).count()
        return Response(count, status=status.HTTP_200_OK)

    def getAverageAge(self, request):
        clients = Client.objects.all()
        sum = 0
        nb = 0
        for client in clients:
            if client.age != -1:
                sum += client.age
                nb += 1
        return Response(sum/nb, status=status.HTTP_200_OK)

    def getAveragePriceByRepresentation(self, request):
        avg = Representation.objects.values('representation').annotate(avgPrice=Avg('prix'))
        return Response(avg, status=status.HTTP_200_OK)

    def getAveragePriceByClient(self, request):
        avg = Reservation.objects.annotate(avgPrice=Avg(F('cle_representation__prix'))).values('email', 'avgPrice')
        return Response(avg, status=status.HTTP_200_OK)

    def getAllStats(self, request):
        stats = {
            'reservationCount': self.getReservationCount(request).data,
            'countUniqueBuyers': self.getCountUniqueBuyers(request).data,
            'averageAge': self.getAverageAge(request).data,
            'averagePriceByRepresentation': self.getAveragePriceByRepresentation(request).data,
            'averagePriceByClient': self.getAveragePriceByClient(request).data,
        }

        return Response(stats, status=status.HTTP_200_OK)